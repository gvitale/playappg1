package controllers

import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._

object Application extends Controller {
  val form = Form(
    tuple(
      "firstname" -> text,
      "lastname" -> text
    )
  )

  def index = Action {
    Ok(views.html.index())
  }

  def submit = Action { implicit request =>
    val (fname, lname) = form.bindFromRequest.get
    Ok("Hi %s %s".format(fname, lname))
  }
}