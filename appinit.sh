#!/bin/sh

aws s3 cp Dockerrun.aws.json s3://nxgeneddocker/scalaappd1/1.0/

eb init \
   -a scalaappd1 \
   -l 1.0 \
   -e scalaappd1-env \
   --region us-east-1 \
   -t WebServer::Standard::1.0 \
   -s "64bit Amazon Linux 2014.09 v1.0.10 running Docker 1.3.2"

aws elasticbeanstalk \
     create-application-version \
     --application-name scalaappd1 \
     --version-label 1.0 \
     --source-bundle S3Bucket="nxgeneddocker",S3Key="scalaappd1/1.0/Dockerrun.aws.json"

