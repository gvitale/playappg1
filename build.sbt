enablePlugins(JavaAppPackaging, DockerPlugin)

packageName in Docker          := "playappg1"

version in Docker              := "1.0"

dockerBaseImage                := "dockerfile/java:oracle-java7"

dockerRepository               := Some("nxgened")

dockerExposedPorts in Docker   := Seq(9000)

dockerExposedVolumes in Docker := Seq("/opt/docker/logs")

dockerUpdateLatest             := true

organization                   := "com.nxgened"

name                           := "playappg1"

version                        := "1.0"

scalaVersion                   := "2.10.4"

//mainClass in Compile           := Some("com.nxgened.MainApp")

// Use this only if something special needs to be configured
// bashScriptExtraDefines         += "export JAVA_OPTS='-Xms512M -Xmx512M -Xss1M -XX:MaxPermSize=256M -XX:+UseParallelGC'"
bashScriptExtraDefines         += """addJava "-J-Xms512M -J-Xmx512M -J-Xss1M""""

//bashScriptConfigLocation       := Some("/opt/docker/conf/etc-default")

libraryDependencies ++= {
  Seq(
    //"org.scalatest"     %% "scalatest"    % "2.2.1" % "test",
    //"org.slf4j"         %  "slf4j-api"    % "1.7.7"
    //play libraries
    //% "jdbc" % "anorm" % "cache" % "ws"
  )
}

lazy val root = (project in file(".")).enablePlugins(PlayScala)

